package com.shakingearthdigital.gameservice.codelab.huawei

import android.app.Application
import android.util.Log
import com.huawei.hms.api.HuaweiMobileServicesUtil
import com.huawei.hms.jos.JosApps


class GameServiceApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        HuaweiMobileServicesUtil.setApplication(this);
    }

    override fun onTerminate() {
        super.onTerminate()
    }
}