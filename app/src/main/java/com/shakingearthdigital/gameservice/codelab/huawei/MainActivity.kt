package com.shakingearthdigital.gameservice.codelab.huawei

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import com.huawei.hms.common.ApiException
import com.huawei.hms.jos.JosApps
import com.huawei.hms.jos.games.Games
import com.huawei.hms.jos.games.PlayersClient
import com.huawei.hms.support.hwid.HuaweiIdAuthManager
import com.huawei.hms.support.hwid.request.HuaweiIdAuthParams
import com.huawei.hms.support.hwid.request.HuaweiIdAuthParamsHelper
import com.huawei.hms.support.hwid.result.HuaweiIdAuthResult
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class MainActivity : AppCompatActivity() {
    private var sessionId: String? = null
    private var playerID: String? = null
    private var playersClient: PlayersClient? = null
    private var handler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initGameButton.setOnClickListener {
            initSDK()
        }
        signInGameButton.setOnClickListener {
            signIn()
        }
        getCurrentPlayerButton.setOnClickListener {
            getCurrentPlayer()
        }
        submitPlayerEventBeginButton.setOnClickListener {
            timeReportStart()
            handler.postDelayed(getPlayerCheck, 1000) //start timed getPlayer check
        }
        submitPlayerEventEndButton.setOnClickListener {
            timeReportEnd()
            handler.removeCallbacksAndMessages(null) //remove our getPlayerCheck
        }
        getPlayerExtraInfoButton.setOnClickListener{
            getPlayerExtra()
        }
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int, @Nullable data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 6013) {
            if (null == data) {
                showLog("signIn inetnt is null")
                return
            }
            val jsonSignInResult = data.getStringExtra("HUAWEIID_SIGNIN_RESULT")
            if (TextUtils.isEmpty(jsonSignInResult)) {
                showLog("signIn result is empty")
                return
            }
            try {
                val signInResult =
                    HuaweiIdAuthResult().fromJson(jsonSignInResult)
                if (0 == signInResult.status.statusCode) {
                    showLog("signIn success.")
                    showLog("signIn result: " + signInResult.toJson())
                } else {
                    showLog("signIn failed: " + signInResult.status.statusCode)
                }
            } catch (var7: JSONException) {
                showLog("Failed to convert json from signInResult.")
            }
        }
    }

    fun showLog(log : String){
        Log.d("MainActivity", log)
    }

    fun initSDK(){
        val appsClient = JosApps.getJosAppsClient(this)
        appsClient.init()
        Log.d("", "init success")
    }

    var getPlayerCheck = Runnable {
        getPlayerExtra()
        queueHandler()
    }

    fun queueHandler(immediate: Boolean = false){
        if(immediate)
            handler.post(getPlayerCheck)
        else
            handler.postDelayed(getPlayerCheck, PLAYER_CHECK_DELAY)
    }

    /**
     * When a player signs in, you need to call the HUAWEI ID sign-in verification API. If the verification is successful, call the getcurrentPlayer API to obtain player information.
     * 1. Call the silent sign-in API of the HUAWEI ID sign-in API so that the sign-in page will not be displayed for a game to which a player has already signed in.
     * 2. When silent sign-in fails, it indicates that this is the player's first sign-in and authorization from the player is required. In this case, in the callback, the explicit sign-in API is called to display the sign-in authorization page for sign-in verification.
     * The sign-in API is called in onActivity. Then you can call the API for obtaining player information.
     */
    fun signIn() {
        val authHuaweiIdTask =
            HuaweiIdAuthManager.getService(this, getHuaweiIdParams()).silentSignIn()
        authHuaweiIdTask.addOnSuccessListener { authHuaweiId ->
            Log.i(TAG, "silentsignIn success")
            Log.i(TAG, "display:" + authHuaweiId.displayName)
            getCurrentPlayer()
        }.addOnFailureListener { e ->
            if (e is ApiException) {
                Log.i(
                    TAG,
                    "signIn failed:" + e.statusCode
                )
                Log.i(TAG, "start getSignInIntent")
                //Sign in explicitly. The sign-in result is obtained in onActivityResult.
                val service =
                    HuaweiIdAuthManager.getService(this@MainActivity, getHuaweiIdParams())
                startActivityForResult(service.signInIntent, 6013)
            }
        }
    }

    fun getCurrentPlayer(){ //login
        playersClient = Games.getPlayersClient(this)
        val playerTask = playersClient!!.currentPlayer
        playerTask.addOnSuccessListener { player ->
            playerID = player.playerId
            Log.i(TAG, "getPlayerInfo Success, player info: $playerID")
        }.addOnFailureListener {e ->
            if(e is ApiException){
                Log.e(TAG,"getPlayerInfo failed, status: ${e.statusCode}")
            }
        }
    }

    fun getHuaweiIdParams(): HuaweiIdAuthParams? {
        return HuaweiIdAuthParamsHelper(HuaweiIdAuthParams.DEFAULT_AUTH_REQUEST_PARAM_GAME)
            .createParams()
    }

    /**
     * Report the game addiction prevention event start time.
     */
    private fun timeReportStart() {
        if (playersClient == null) {
            Log.i(TAG,"playersClient is null, please init  playersClient first")
            getCurrentPlayer()
            return
        }
        if (playerID == null) {
            Log.i(TAG,"playerID is null, please getcurrentPlayer login first")
            getCurrentPlayer()
            return
        }
        val uid: String = UUID.randomUUID().toString()
        val task = playersClient!!.submitPlayerEvent(playerID, uid, "GAMEBEGIN")
        task.addOnSuccessListener{ jsonRequest ->
            try {
                val data = JSONObject(jsonRequest)
                sessionId = data.getString("transactionId")
                queueHandler(true)
            } catch (e: JSONException) {
                showLog("parse jsonArray meet json exception")
                return@addOnSuccessListener
            }
            showLog("submitPlayerEvent traceId: $jsonRequest")
        }.addOnFailureListener { e ->
            if (e is ApiException) {
                val result = "rtnCode:" + e.statusCode
                showLog(result)
            }
        }
    }

    /**
     * Report the game addition prevention end time.
     * sessionId: The session ID is returned in the callback for a game entering event and will be used to calculate the played time when a game exit event is reported.
     */
    private fun timeReportEnd() {
        if (playersClient == null) {
            Log.i(TAG, "playersClient is null, please init  playersClient first")
            getCurrentPlayer()
            return
        }
        if (playerID == null) {
            Log.i(TAG,"playerID is null, please getcurrentPlayer login first")
            getCurrentPlayer()
            return
        }
        if (sessionId == null) {
            Log.i(TAG, "sessionId is null, please submitPlayerEvent Begin  first")
            getCurrentPlayer()
            return
        }
        val task = playersClient!!.submitPlayerEvent(playerID, sessionId, "GAMEEND")
        task.addOnSuccessListener { s -> showLog("submitPlayerEvent traceId: $s") }
            .addOnFailureListener { e ->
                if (e is ApiException) {
                    val result = "rtnCode:" + e.statusCode
                    showLog(result)
                }
            }
    }

    /**
     * Query game addiction prevention event reporting details.
     * If a player is a minor, call PlayerExtraInfo.getPlayerDuration to query the latest accumulated played time of the player on the current day.
     * Your game needs to perform game addiction prevention on minor players based on the accumulated played time. You need to implement the prevention logic based on your game.
     */
    private fun getPlayerExtra() {
        if (playersClient == null) {
            Log.i(TAG,"playersClient is null, please init  playersClient first")
            getCurrentPlayer()
            return
        }
        if (sessionId == null) {
            Log.i(TAG,"sessionId is null, please submitPlayerEvent Begin  first")
            getCurrentPlayer()
            return
        }
        val task = playersClient!!.getPlayerExtraInfo(sessionId)
        task.addOnSuccessListener { extra ->
            if (extra != null) {
                showLog("IsRealName: " + extra.isRealName + ", IsAdult: " + extra.isAdult
                            + ", PlayerId: " + extra.playerId + ", PlayerDuration: " + extra.playerDuration)
            } else {
                showLog("Player extra info is empty.")
            }
        }.addOnFailureListener { e ->
            if (e is ApiException) {
                val result = "rtnCode:" + e.statusCode
                showLog(result)
            }
        }
    }

    companion object{
        const val TAG = "MainActivity"
        const val PLAYER_CHECK_DELAY = 10*1000*60L //10 minutes
    }
}
